//
//  NSViewController.h
//  SymbolFinder
//
//  Created by Dave Carlton on 08/15/10.
//  Copyright 2010 PolyMicro Systems. All rights reserved.
//

#ifdef PMUtility
#import <Cocoa/Cocoa.h>

@interface NSViewController (PMViewControllerExtensions) 


@end

#endif

#ifdef PMUtilityIOS
#import <UIKit/UIKit.h>
#endif

