//
//	File: NSOutlineView_Extensions.h
//  Abstract: Interface file for NSOutlineView_Extensions.m
//
//  Created by Dave Carlton on 08/09/10.
//  Copyright (c) 2010 PolyMicro Systems. All rights reserved.
//
 
#ifdef PMUtility
#import <Cocoa/Cocoa.h>
#endif

#ifdef PMUtilityIOS
#import <UIKit/UIKit.h>
#endif
 
@interface NSOutlineView (PMExtension)
 
@property (NS_NONATOMIC_IOSONLY, readonly, copy) NSArray *allSelectedItems;
- (void)selectItems:(NSArray*)items byExtendingSelection:(BOOL)extend;
- (void)selectItems:(NSArray*)items;
- (void)extendSelectItems:(NSArray*)items;
@property (NS_NONATOMIC_IOSONLY, readonly) unsigned long lastSelectedRow;
- (unsigned long) nextRow:(int)curRow wrapOK:(BOOL)wrapFlag;
- (unsigned long) nextSelectedRow:(int)curSelectedRow wrapOK:(BOOL)wrapFlag;
@property (NS_NONATOMIC_IOSONLY, readonly) unsigned long firstSelectedRow;
 
@end
 
 
